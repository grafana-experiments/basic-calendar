# grafana plugins calendar


This is a basic grafana dashboard which shows a few panels on a dashboard.  The demo uses container to run the grafana instance.  The main panel shows dates when [grafana plugins](https://grafana.com/grafana/plugins/) were updated.  The demo uses [this JSON API plugin](https://grafana.com/grafana/plugins/marcusolsson-json-datasource/) and [calendar](https://grafana.com/grafana/plugins/marcusolsson-calendar-panel/)

For additional demonstration purposes, there are clocks from different timezones displayed:
* New York, USA
* Dublin, Ireland
* Tel Aviv, Israel


There is also a panel showing rotation periods of planets in the Star Wars galaxy, as documented on [grafana blog](https://grafana.com/blog/2023/08/30/grafana-json-api-how-to-import-third-party-data-sources-in-grafana-cloud/).  This shows how to define a second data source using the same plugin.


# JSON data sources
* https://grafana.com/api/plugins
* https://swapi.dev/api/planets/?format=json

## JSON queries
For grafana plugins calendar, one field has to be date/time
- ```$.items[*].name```
- ```$.items[*].description```
- ```$.items[*].updatedAt```

For the Star Wars planets data, I use
- ```$..name```
- ```$..rotation_duration```


## Quickstart

If you have docker available, it is as easy as :
```
docker compose up -d 
```

Then open [http://localhost:3000](http://localhost:3000)
Initial user/password is **admin/admin**

Wait 1 minute (the plugins have to install and dashboard pull data from 2 locations) and you will see the following ![image](images/dashboard.png)


## Contribute

Contributions are more than welcome! Feel free to submit a MR